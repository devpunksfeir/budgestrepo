//
//  SignUpViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 31/10/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var rePassword: UITextField!
    @IBOutlet weak var nickname: UITextField!
    @IBOutlet weak var birthday: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let toolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
        
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        
        toolBar.barStyle = UIBarStyle.BlackTranslucent
        
        toolBar.tintColor = UIColor.whiteColor()
        
        toolBar.backgroundColor = UIColor.blackColor()
        
        
        let todayBtn = UIBarButtonItem(title: "Aujourd'hui", style: UIBarButtonItemStyle.Plain, target: self, action: "tappedToolBarBtn:")
        
        let okBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "donePressed:")
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
        
        label.font = UIFont(name: "Helvetica", size: 12)
        
        label.backgroundColor = UIColor.clearColor()
        
        label.textColor = UIColor.whiteColor()
        
        label.text = "Date de naissance"
        
        label.textAlignment = NSTextAlignment.Center
        
        let textBtn = UIBarButtonItem(customView: label)
        
        toolBar.setItems([todayBtn,flexSpace,textBtn,flexSpace,okBarBtn], animated: true)
        
        birthday.inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func SigneUpTapped(sender: UIButton) {
        
        let usernameValue:NSString = userName.text!
        let passwordValue:NSString = password.text!
        let repasswordValue:NSString = rePassword.text!
        
        if ( usernameValue.isEqualToString("") || passwordValue.isEqualToString("")
            || repasswordValue.isEqualToString("")) {
            
                AlertManager.showAlert("Erreur lors de l'enregistrement", message: "Veuillez saisir un login et un mot de passe", controler: self)
                
        } else if(!passwordValue.isEqualToString(repasswordValue as String) ){
            
            AlertManager.showAlert("Erreur lors de l'enregistrement", message: "Les mots de passe saisis sont différents", controler: self)
            
        } else {
            
            let profile : Profile = getProfileFromUI()
            
            RestWebServiceConnector.createProfile({ (json, err) -> Void in
                if((err) == nil){
                    do {
                        let dic: NSDictionary = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        
                        CommonProperties.profile = Profile.fromJSON(dic)
                        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                            let vc : UITabBarController = self.storyboard!.instantiateViewControllerWithIdentifier("MainTabControler") as! UITabBarController
                            self.presentViewController(vc, animated: true, completion: nil)
                        }
                        
                        
                    } catch {
                        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                            AlertManager.showAlert("Erreur", message: "Erreur lors de la récupération du profile", controler: self)
                        }
                    }
                    
                    
                }
            }, profile: profile)
            
        }
    }
    
    func getProfileFromUI() ->Profile{
        let profile : Profile = Profile()
        profile.Nickname = nickname.text!
        let credential : Credential = Credential()
        credential.id = -1
        credential.login = userName.text!
        credential.password = password.text!
        profile.LoginInfo = credential
        let user : User = User()
        profile.user = user
        
        return profile
    }
    
    @IBAction func birthdayEditing(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        birthday.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    func donePressed(sender: UIBarButtonItem) {
        
        birthday.resignFirstResponder()
        
    }
    
    func tappedToolBarBtn(sender: UIBarButtonItem) {
        
        let dateformatter = NSDateFormatter()
        
        dateformatter.dateStyle = NSDateFormatterStyle.ShortStyle
        
        dateformatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        birthday.text = dateformatter.stringFromDate(NSDate())

        birthday.resignFirstResponder()
    }
    
    
}
