//
//  ProfileViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 08/12/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate{

    @IBOutlet var nicknameTextfield: UITextField!
    
    @IBOutlet var loginTextfield: UITextField!
    
    @IBOutlet var lastnameTextfield: UITextField!
    
    @IBOutlet var firstnameTextfield: UITextField!
    
    @IBOutlet var birthdayTextfield: UITextField!
    
   
    
    @IBOutlet weak var imgImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

    
        circleCrop()
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("imageTapped:"))
        imgImage.userInteractionEnabled = true
        imgImage.addGestureRecognizer(tapGestureRecognizer)
        
        loadProfile()
        
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func loadProfile() {
        nicknameTextfield.text = CommonProperties.profile.Nickname
        loginTextfield.text = CommonProperties.profile.LoginInfo.login
        lastnameTextfield.text = CommonProperties.profile.user.lastName
        firstnameTextfield.text = CommonProperties.profile.user.firstName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func imageTapped(sender: AnyObject) {
        
        let picker = UIImagePickerController()
        picker.sourceType = .PhotoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        self.presentViewController(picker, animated: true, completion: nil)
        
    }
    
    func circleCrop(){
        self.imgImage.layer.cornerRadius = self.imgImage.frame.size.width / 2
        self.imgImage.clipsToBounds = true
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: AnyObject]) {
        
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            self.imgImage.image = possibleImage
            
            
            let data = UIImageJPEGRepresentation(possibleImage, 1.0)
            CommonProperties.profile.picture = data!
        } else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            self.imgImage.image = possibleImage
            let data = UIImageJPEGRepresentation(possibleImage, 1.0)
            CommonProperties.profile.picture = data!
        } else {
            return
        }
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func SaveButtonTapped(sender: UIButton) {
        let profile : Profile = Profile()
        profile.id = CommonProperties.profile.id
        profile.Nickname = nicknameTextfield.text!
        profile.user.lastName = lastnameTextfield.text!
        profile.user.firstName = firstnameTextfield.text!
        profile.LoginInfo.login = loginTextfield.text!
        profile.picture = CommonProperties.profile.picture
        
        RestWebServiceConnector.updateProfile({(json,error) -> Void in
            CommonProperties.profile.Nickname = self.nicknameTextfield.text!
            CommonProperties.profile.user.lastName = self.lastnameTextfield.text!
            CommonProperties.profile.user.firstName = self.firstnameTextfield.text!
            CommonProperties.profile.LoginInfo.login = self.loginTextfield.text!
            }, profile: profile)
        
    }
    
    @IBAction func done(segue:UIStoryboardSegue) {
        
    }
    
    @IBAction func cancel(segue:UIStoryboardSegue) {
        
    }
    @IBAction func LogoutTapped(sender: UIButton) {
        CommonProperties.profile = Profile()
        let vc : UINavigationController = self.storyboard!.instantiateViewControllerWithIdentifier("StartControler") as! UINavigationController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
}
