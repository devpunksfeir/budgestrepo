//
//  AccountViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 02/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class AccountViewController: UITableViewController {
    
    var bankAccounts : [BankAccount] = CommonProperties.profile.accounts
    var deleteAccount: NSIndexPath? = nil
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get bank account rest
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        let itemToMove = bankAccounts[fromIndexPath.row]
        bankAccounts.removeAtIndex(fromIndexPath.row)
        bankAccounts.insert(itemToMove, atIndex: toIndexPath.row)
    }
    
    func confirmDelete(account: BankAccount) {
        let alert = UIAlertController(title: "Supprission de compte en banque", message: "Voulez vous supprimer le comte \(account.name)?", preferredStyle: .ActionSheet)
        
        let DeleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: handleDeleteAccount)
        let CancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: cancelDeleteAccount)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        // Support display in iPad
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func handleDeleteAccount(alertAction: UIAlertAction!) -> Void {
        if let indexPath = deleteAccount {
            tableView.beginUpdates()
            
            RestWebServiceConnector.deleteBankAccount({(json, error) -> Void in
                if(error == nil ){
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        self.bankAccounts.removeAtIndex(indexPath.row)
                        
                        CommonProperties.profile.accounts.removeAtIndex(indexPath.row)
                        
                        // Note that indexPath is wrapped in an array:  [indexPath]
                        self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                        
                        self.deleteAccount = nil
                        
                        self.tableView.endUpdates()
                    }
                }
            }, bankaccountId: bankAccounts[indexPath.row].id)
        }
    }
    
    func cancelDeleteAccount(alertAction: UIAlertAction!) {
        deleteAccount = nil
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankAccounts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("accountCell", forIndexPath: indexPath) as! BankAccountCell
        cell.bankAccount = bankAccounts[indexPath.row]
        cell.titleLabel.text = bankAccounts[indexPath.row].name
        cell.bankNameLabel.text = bankAccounts[indexPath.row].bank.name
        let tot = bankAccounts[indexPath.row].operations.map({$0.amount}).reduce(0, combine: +)
        
        cell.ammountLabel.text =  String(bankAccounts[indexPath.row].amount + tot) + " " + bankAccounts[indexPath.row].devise
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete") { (action, indexPath) in
            self.deleteAccount = indexPath
            let account = self.bankAccounts[indexPath.row]
            self.confirmDelete(account)
        }
        
        let edit = UITableViewRowAction(style: .Normal, title: "Edit") { (action, indexPath) in
            let account = self.bankAccounts[indexPath.row]
            self.editAccount(account)
        }
        
        edit.backgroundColor = UIColor.blueColor()
        
        return [delete, edit]
    }
    
    func editAccount(account : BankAccount){
        let vc : AddAccountViewController = self.storyboard!.instantiateViewControllerWithIdentifier("AddBankAccount") as! AddAccountViewController
        vc.bankAccount = account
        vc.openMode = OpenMode.Edit
        vc.modalPresentationStyle = .Popover
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func done(segue:UIStoryboardSegue) {
        let vc = segue.sourceViewController as! AddAccountViewController
        switch vc.openMode {
            
        case OpenMode.Add :
            
            
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                CommonProperties.profile.accounts.append(vc.bankAccount)
                self.bankAccounts.append(vc.bankAccount)
                let indexPath = NSIndexPath(forRow: self.bankAccounts.count-1, inSection: 0)
                self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                
                self.tableView.reloadData()
            }
            
        default : break
            
        }
        
        
        
    }
    
    @IBAction func cancel(segue:UIStoryboardSegue) {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        switch segue.identifier {
        case "ShowAddAccount"? :
            let svc = segue.destinationViewController as! AddAccountViewController
            svc.openMode = OpenMode.Add
            
        case "DisplayAccountSegue"? :
            let cell = sender as! BankAccountCell
            let svc = segue.destinationViewController as! AccountOperationGroupController
            svc.bankAccount = cell.bankAccount
        default : break
            
        }
    }
    
    
}
