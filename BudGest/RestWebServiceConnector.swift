//
//  RestWebServiceConnector.swift
//  BudGest
//
//  Created by Renaud Chardin on 22/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class RestWebServiceConnector {
    
    static let prefix = NSBundle.mainBundle().objectForInfoDictionaryKey("URLBudGestWSPrefix") as! String
    
    static func createBank(callback: (NSData,String?) -> Void, bank : Bank) {
        //budgestws
        let rest : RestManager = RestManager()
        let data = Bank.toJSON(bank)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/createbank", body: data)
    }
    
    static func getAllBanks(callback: (NSData,String?) -> Void) {
        
        let rest : RestManager = RestManager()
        rest.makeHTTPGetRequest(callback, url: "\(prefix)/BudGest/GetBanks")
        
    }
    
    static func getBankAccountByUserId(callback: (NSData,String?) -> Void, userId : Int) {
        let rest : RestManager = RestManager()
        rest.makeHTTPGetRequest(callback, url: "\(prefix)/BudGest/getuserbankaccounts/\(userId)")

    }
    
    static func createBankAccount(callback: (NSData,String?) -> Void, bankaccount : BankAccount) {
        let rest : RestManager = RestManager()
        let data = BankAccount.toJSON(bankaccount)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/createbankaccount", body: data)
    }
    
    static func deleteBankAccount(callback: (NSData,String?) -> Void, bankaccountId : Int) {
        let rest : RestManager = RestManager()
        rest.makeHTTPGetRequest(callback, url: "\(prefix)/BudGest/deletebankaccount/\(bankaccountId)")
    }
    
    static func updateBankAccount(callback: (NSData,String?) -> Void, bankaccount : BankAccount) {
        let rest : RestManager = RestManager()
        let data = BankAccount.toJSON(bankaccount)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/updatebankaccount", body: data)
    }
    
    static func createProfile(callback: (NSData,String?) -> Void, profile : Profile){
        
        let rest : RestManager = RestManager()
        let data = Profile.toJSON(profile)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/createprofile", body: data)
        
        
    }
    
    static func getProfileByLogin(callback: (NSData,String?) -> Void, login : String, pwd : String){
        let rest : RestManager = RestManager()
        rest.makeHTTPGetRequest(callback, url: "\(prefix)/BudGest/getprofile/\(login)")
        
    }
    
    static func updateProfile(callback: (NSData,String?) -> Void, profile : Profile){
        let rest : RestManager = RestManager()
        let data = Profile.toJSON(profile)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/updateprofile", body: data)
    }
    
    static func updatePassword(callback: (NSData,String?) -> Void, credetial : Credential){
        let rest : RestManager = RestManager()
        let data = Credential.ToJSON(credetial)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/updatepassword", body: data)
    }
    
    static func createOperation(callback: (NSData,String?) -> Void, operation : Operation){
        
        let rest : RestManager = RestManager()
        let data = Operation.toJSON(operation)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/createoperation", body: data)
        
        
    }
    
    static func deleteOperation(callback: (NSData,String?) -> Void, operationId : Int){
        let rest : RestManager = RestManager()
        rest.makeHTTPGetRequest(callback, url: "\(prefix)/BudGest/deleteoperation/\(operationId)")
        
    }
    
    static func updateOperation(callback: (NSData,String?) -> Void, operation : Operation){
        
        let rest : RestManager = RestManager()
        let data = Operation.toJSON(operation)
        rest.makeHTTPPostRequest(callback, url: "\(prefix)/BudGest/updateoperation", body: data)
        
        
    }
    
}