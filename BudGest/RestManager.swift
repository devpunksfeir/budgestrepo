import Foundation
import UIKit

class RestManager: NSObject, NSURLConnectionDataDelegate {
    
    enum Path {
        case SIGIN_IN
        case GET_ROOMS
        case GET_MESSAGES
        case CREATE_MESSAGE
    }
    
    typealias APICallback = ((AnyObject?, NSError?) -> ())
    let responseData = NSMutableData()
    var statusCode:Int = -1
    var callback: APICallback! = nil
    var path: Path! = nil
    
    
    // private
    func makeHTTPGetRequest( callback: (NSData,String?) -> Void, url: NSString) {
        
        if Reachability.isConnectedToNetwork(){
            
            EZLoadingActivity.show("Loading ...", disableUI: true)
            
            let request = NSMutableURLRequest(URL: NSURL(string: url as String)!)
            request.HTTPMethod = "GET"
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(request){
                (data,response,error) -> Void in
                if (error != nil){
                    EZLoadingActivity.hide(success: false, animated: true)
                    self.showAlert("Erreur de communication", message: "Erreur lors de la récupération depuis le serveur")
                    print(error!.description)
                    //callback(data!,error!.description)
                } else {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            EZLoadingActivity.hide()
                            callback(data!,nil)
                        } else {
                            EZLoadingActivity.hide(success: false, animated: true)
                            self.showAlert("Erreur de communication", message: "Erreur lors de la récupération depuis le serveur, Statut HTTP :" + String(httpResponse.statusCode))
                        }

                    }
                }
            }
            task.resume()
            
        } else {
            self.showAlert("Pas de connection internet", message: "Vous n'êtes pas connecté à internet, veuillez vérifier votre connexion" )
        }
        
    }
    
    func makeHTTPPostRequest(callback: (NSData,String?) -> Void, url: NSString, body: [String : AnyObject]) {
        
        if Reachability.isConnectedToNetwork(){
            
            EZLoadingActivity.show("Uploading ...", disableUI: true)
            
            var data : NSData = NSData()
            do{
                data = try NSJSONSerialization.dataWithJSONObject(body, options:[])
            } catch {
                
            }
            
            let request = NSMutableURLRequest(URL: NSURL(string: url as String)!)
            request.HTTPMethod = "POST"
            request.addValue("application/json",forHTTPHeaderField: "Content-Type")
            request.addValue("application/json",forHTTPHeaderField: "Accept")
            request.HTTPBody = data
            
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(request){
                (data,response,error) -> Void in
                if (error != nil){
                    self.showAlert("Erreur de communication", message: "Erreur lors de l'envoi vers le serveur")
                    EZLoadingActivity.hide(success: false, animated: true)
                    //callback(data!,error?.localizedDescription)
                } else {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            callback(data!,nil)
                            EZLoadingActivity.hide()
                        } else {
                            self.showAlert("Erreur de communication", message: "Erreur lors de l'envoi vers le serveur, Statut HTTP :" + String(httpResponse.statusCode))
                            EZLoadingActivity.hide(success: false, animated: true)
                        }
                    }
                }
            }
            task.resume()
        } else {
                self.showAlert("Pas de connection internet", message: "Vous n'êtes pas connecté à internet, veuillez vérifier votre connexion" )
            
        }
    }
    
    
    func showAlert(title : String, message : String) {
        if let topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
            dispatch_async(dispatch_get_main_queue()) { Void in
                AlertManager.showAlert(title, message: message, controler: topController )
            }
        }
    }
}