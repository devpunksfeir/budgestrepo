//
//  LoginViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 31/10/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit
import RestManager

class LoginViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var pwdValue: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func loginTapped(sender: UIButton) {
        
        let username:NSString = userName.text!
        let password:NSString = pwdValue.text!
        
        if ( username.isEqualToString("") || password.isEqualToString("") ) {
            
            let alertView = UIAlertController(title: "Erreur de connection", message: "Veuillez saisir un login et un mot de passe", preferredStyle: UIAlertControllerStyle.Alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertView, animated: true, completion: nil)
        } else {
            
            //RestManager.sharedInstance.makeHTTPGetRequest("",)
            
            // Call rest login 
            let signInOk = (username.isEqualToString("charon") && password.isEqualToString("pwd"))
            
            
            if(signInOk) {
                let vc : UITabBarController = self.storyboard!.instantiateViewControllerWithIdentifier("MainTabControler") as! UITabBarController
                self.presentViewController(vc, animated: true, completion: nil)
                
            }

        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
