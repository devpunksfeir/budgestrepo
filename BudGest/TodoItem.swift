//
//  TodoItem.swift
//  TodoNotificationsTutorial
//
//  Created by Jason Newell on 1/28/15.
//  Copyright (c) 2015 Jason Newell. All rights reserved.
//

import Foundation

struct TodoItem {
    var title: String
    var deadline: NSDate
    var UUID: String
    var amount : Double
    
    init(deadline: NSDate, title: String, UUID: String, amount : Double) {
        self.deadline = deadline
        self.title = title
        self.UUID = UUID
        self.amount = amount
    }
    
    var isOverdue: Bool {
        return (NSDate().compare(self.deadline) == NSComparisonResult.OrderedDescending) // deadline is earlier than current date
    }
}