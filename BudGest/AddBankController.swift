//
//  AddBankController.swift
//  BudGest
//
//  Created by Renaud Chardin on 22/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class AddBankController: UIViewController {
    
    var bank : Bank = Bank()
    
    @IBOutlet var TitleLabel: UILabel!
    
    @IBOutlet var BankNameTextbox: UITextField!

    @IBOutlet var BicTextbox: UITextField!
    
    @IBOutlet var BankAddressTextbox: UITextField!
    
    @IBOutlet var LoginTextbox: UITextField!
    
    @IBOutlet var PasswordTextbox: UITextField!
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        
        if identifier == "SaveBank" {
            bank.name = BankNameTextbox.text!
            bank.bic = BicTextbox.text!
            bank.address = BankAddressTextbox.text!
            
            RestWebServiceConnector.createBank({(json,error)  -> Void in
                if(error == nil){
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    self.performSegueWithIdentifier(identifier, sender: self)
                    }
                }
                }, bank: bank)
            return false
        }
        return true
    }
    
    
    
    
}
