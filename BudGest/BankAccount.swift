//
//  BankAccount.swift
//  BudGest
//
//  Created by Renaud Chardin on 04/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class BankAccount{
    
    var profileId : Int = -1
    var id : Int = -1
    var name : String = ""
    var devise : String = ""
    var iban : String = ""
    var type : String = ""
    var amount : Double = -1.0
    var overdraft : Double = 0.0
    var ease : Double = 0.0
    var earnings : Double = 0.0
    var user : User = User()
    var bank : Bank = Bank()
    var operations : [Operation] = [Operation]()
    
    init(){
        
    }
    
    init(id :Int,name : String,devise : String, iban : String, type : String, amount : Double, overdraft :Double, ease : Double, earnings : Double, user : User, bank : Bank,operations : [Operation]){
        
        self.id = id
        self.name = name
        self.devise = devise
        self.iban = iban
        self.type = type
        self.amount = amount
        self.overdraft = overdraft
        self.ease = ease
        self.earnings = earnings
        self.user = user
        self.bank = bank
        self.operations = operations
        
    }
    
    static func fromJSON(data : NSDictionary) -> BankAccount{
        
        let id = data.valueForKey("id") as! Int
        let name = data.valueForKey("name") as! String
        let devise = data.valueForKey("devise") as! String
        let iban = data.valueForKey("devise") as! String
        let type = data.valueForKey("type") as! String
        let amount = data.valueForKey("amount") as! Double
        let overdraft = data.valueForKey("overdraft") as! Double
        let ease = data.valueForKey("ease") as! Double
        let earnings = data.valueForKey("earnings") as! Double
        var user = User()
        if((data.valueForKey("user") as? NSDictionary) != nil){
            user = User.fromJSON(data.valueForKey("user") as! NSDictionary)
        }
        var bank = Bank()
        if((data.valueForKey("bank") as? NSDictionary) != nil){
            bank = Bank.fromJSON(data.valueForKey("bank") as! NSDictionary)
        }
        
        var operations = [Operation]()
        if((data.valueForKey("operations") as? NSArray) != nil){
            for operation in (data.valueForKey("operations") as! NSArray){
                if((operation as? NSDictionary) != nil){
                    operations.append(Operation.fromJSON(operation as! NSDictionary))
                }
            }
        }
        
        return BankAccount(id: id, name: name, devise: devise, iban: iban, type: type, amount: amount, overdraft: overdraft, ease: ease, earnings: earnings, user: user, bank: bank, operations: operations)
    }
    
    static func toJSON(bankAccount : BankAccount) -> [String: AnyObject]{
        
        var operations  = [NSDictionary]()
        for o in bankAccount.operations {
            operations.append(Operation.toJSON(o))
        }
        
        let jsonObject : [String: AnyObject] = [
            "id" : bankAccount.id,
            "name" : bankAccount.name,
            "devise" : bankAccount.devise,
            "iban" : bankAccount.iban,
            "type" : bankAccount.type,
            "amount" : bankAccount.amount,
            "overdraft" : bankAccount.overdraft,
            "ease" : bankAccount.ease,
            "earnings" : bankAccount.earnings,
            "user" : User.toJSON(bankAccount.user),
            "bank" : Bank.toJSON(bankAccount.bank),
            "profileId" : bankAccount.profileId
        ]
        return jsonObject
    }
    
}