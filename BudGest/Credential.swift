//
//  Credential.swift
//  BudGest
//
//  Created by Renaud Chardin on 03/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class Credential {
    
    var id : Int = -1
    var login : String = ""
    var password : String = ""
    var pwdkey : String = ""
    
    init(){
        
    }
    
    init(id : Int, login : String, pwd : String, key : String ){
        self.id = id
        self.login = login
        self.password = pwd
        self.pwdkey = key
    }
    
    static func FromJSON(json : NSDictionary) -> Credential {
        
        let id = json.valueForKey("id") as! Int
        let login = json.valueForKey("Login") as! String
        let password = json.valueForKey("Password") as! String
        let key = json.valueForKey("PwdKey") as! String
        
        return Credential(id: id,login: login,pwd: password, key: key)
    }
    
    static func ToJSON(credential : Credential) -> [String: AnyObject] {
        let jsonObject : [String: AnyObject] =  [
            "id" : credential.id,
            "Login" : credential.login,
            "Password" : credential.password,
            "PwdKey" : credential.pwdkey
        ]
        
        return jsonObject
        
    }
    
}