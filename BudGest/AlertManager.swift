//
//  AlertManager.swift
//  BudGest
//
//  Created by Renaud Chardin on 15/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation
import UIKit

class AlertManager {
    
    static func showAlert(title : String, message : String, controler : UIViewController){
        let alertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        
        controler.presentViewController(alertView, animated: true, completion: nil)
    }
    
}