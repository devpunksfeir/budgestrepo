//
//  AddAccountViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 16/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class AddAccountViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var openMode : OpenMode = OpenMode.Add
    
    var bankAccount : BankAccount = BankAccount()
    
    var banks : [Bank] = []
    
    @IBOutlet var TitleLabel: UILabel!
    
    @IBOutlet var AccountNameTextBox: UITextField!
    
    @IBOutlet var IbanTextBox: UITextField!
    
    @IBOutlet var BankTextBox: UITextField!
    
    @IBOutlet var DeviseTextBox: UITextField!
    
    @IBOutlet var AmountTextBox: UITextField!
    
    @IBOutlet var EarningsTextBox: UITextField!
    
    @IBOutlet var TypeTextBox: UITextField!
    
    @IBOutlet var OverdraftTextBox: UITextField!
    
    @IBOutlet var EaseTextBox: UITextField!
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return banks.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return banks[row].name
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        BankTextBox.text = "\(banks[row].name)"
        bankAccount.bank = banks[row]
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func loadBanks(){
        
        banks = []
        
        
        RestWebServiceConnector.getAllBanks({ (json, err) -> Void in
            
            do{
                
                let dic: NSArray = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                
                for element  in dic {
                    let b : Bank = Bank.fromJSON((element as! NSDictionary))
                    if(!b.name.isEmpty){
                        self.banks.append(b)
                    }
                }
            } catch{
                
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.AmountTextBox.delegate = self
        self.EarningsTextBox.delegate = self
        self.EaseTextBox.delegate = self
        self.OverdraftTextBox.delegate = self
        
        loadBanks()
        
        addToolBar()
        
        switch openMode {
            
        case OpenMode.Add :
            TitleLabel.text = "Ajouter un compte en banque"
            DeviseTextBox.text = "Eur"
        case OpenMode.Edit :
            TitleLabel.text = "Modifier le compte en banque"
            bindBankAcount()
            BankTextBox.enabled = false
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    func textField(textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        // We ignore any change that doesn't add characters to the text field.
        // These changes are things like character deletions and cuts, as well
        // as moving the insertion point.
        //
        // We still return true to allow the change to take place.
        if string.characters.count == 0 {
            return true
        }
        
        // Check to see if the text field's contents still fit the constraints
        // with the new content added to it.
        // If the contents still fit the constraints, allow the change
        // by returning true; otherwise disallow the change by returning false.
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
       return prospectiveText.containsOnlyCharactersIn("-0123456789.")
        
  
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func bindBankAcount() {
        AccountNameTextBox.text = bankAccount.name
        IbanTextBox.text = bankAccount.iban
        BankTextBox.text = bankAccount.bank.name
        DeviseTextBox.text = bankAccount.devise
        AmountTextBox.text = String(bankAccount.amount)
        EarningsTextBox.text = String(bankAccount.earnings)
        OverdraftTextBox.text = String(bankAccount.overdraft)
        TypeTextBox.text = bankAccount.type
        EaseTextBox.text = String(bankAccount.ease)
    }
    
    func addToolBar(){
        let toolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
        
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        
        toolBar.barStyle = UIBarStyle.BlackTranslucent
        
        toolBar.tintColor = UIColor.whiteColor()
        
        toolBar.backgroundColor = UIColor.blackColor()
        
        
        let okBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "donePressed:")
        
        let addBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addPressed:")
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
        
        label.font = UIFont(name: "Helvetica", size: 14)
        
        label.backgroundColor = UIColor.clearColor()
        
        label.textColor = UIColor.whiteColor()
        
        label.text = "Choisir une banque"
        
        label.textAlignment = NSTextAlignment.Center
        
        let textBtn = UIBarButtonItem(customView: label)
        
        toolBar.setItems([addBarBtn,flexSpace,textBtn,flexSpace,okBarBtn], animated: true)
        
        BankTextBox.inputAccessoryView = toolBar
    }
    
    func addPressed(sender: UIBarButtonItem) {
        let vc : AddBankController = self.storyboard!.instantiateViewControllerWithIdentifier("AddBankView") as! AddBankController
        vc.modalPresentationStyle = .Popover
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    func donePressed(sender: UIBarButtonItem) {
        
        let pickerView:UIPickerView = BankTextBox.inputView as! UIPickerView
        let i = pickerView.selectedRowInComponent(0)
        BankTextBox.text = "\(banks[i].name)"
        bankAccount.bank = banks[i]
        BankTextBox.resignFirstResponder()
        
    }
    
    @IBAction func Done(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func SelectBankBegin(sender: UITextField) {
        let pickerView:UIPickerView = UIPickerView()
        
        
        sender.inputView = pickerView
        pickerView.dataSource = self
        pickerView.delegate = self
        
        if let i = banks.indexOf({$0.id == bankAccount.bank.id}){
            pickerView.selectRow(i, inComponent: 0, animated: true)
        }
        
        pickerView.targetForAction(Selector("bankSelected"),
            withSender : sender)
        
    }
    
    @IBAction func done(segue:UIStoryboardSegue) {
        loadBanks()
    }
    
    @IBAction func cancel(segue:UIStoryboardSegue) {
        
    }
    
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "SaveNewAccount" {
            
            
            bankAccount.name = AccountNameTextBox.text!
            bankAccount.iban = IbanTextBox.text!
            bankAccount.devise = DeviseTextBox.text!
            if !AmountTextBox.text!.isEmpty {
                bankAccount.amount = Double(AmountTextBox.text!)!
            }
            if !EarningsTextBox.text!.isEmpty {
                bankAccount.earnings = Double(EarningsTextBox.text!)!
            }
            bankAccount.type = TypeTextBox.text!
            if !EaseTextBox.text!.isEmpty {
                bankAccount.ease = Double(EaseTextBox.text!)!
            }
            if !OverdraftTextBox.text!.isEmpty {
                bankAccount.overdraft = Double(OverdraftTextBox.text!)!
            }
            
            bankAccount.user = CommonProperties.profile.user
            bankAccount.profileId = CommonProperties.profile.id
            
            
            
            switch openMode {
                
            case OpenMode.Add :
                // call rest add
                //TitleLabel.text = "Ajouter un compte en banque"
                RestWebServiceConnector.createBankAccount({(json,error) -> Void in
                    if(error == nil){
                        let datastring = NSString(data: json, encoding:NSUTF8StringEncoding)
                        self.bankAccount.id = Int(datastring as! String)!
                        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                            self.performSegueWithIdentifier("SaveNewAccount", sender: self)
                        }
                    }
                    
                    }, bankaccount: bankAccount)
            case OpenMode.Edit :
                //  call
                RestWebServiceConnector.updateBankAccount({(json,error) -> Void in
                    if(error == nil){
                        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                            self.performSegueWithIdentifier("SaveNewAccount", sender: self)
                        }
                    }
                    
                    }, bankaccount: bankAccount)
            }
            return false
        }
        return true
    }
    
    
}
