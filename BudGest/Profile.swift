//
//  Profile.swift
//  BudGest
//
//  Created by Renaud Chardin on 05/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class Profile{
    
    var id: Int = -1
    var Nickname : String = ""
    var picture : NSData = NSData()
    var user : User = User()
    var LoginInfo : Credential = Credential()
    var accounts : [BankAccount] = [BankAccount]()
    
    init(){
        
    }
    
    init(id: Int,Nickname : String,picture : NSData,user : User,LoginInfo : Credential,accounts : [BankAccount]){
        self.id = id
        self.Nickname = Nickname
        self.picture = picture
        self.user = user
        self.LoginInfo = LoginInfo
        self.accounts = accounts
    }
    
    
    static func fromJSON(data : NSDictionary) -> Profile {
        let id = data.valueForKey("id") as! Int
        if (id != -1){
           let nickname = data.valueForKey("Nickname") as! String
        
        var picture = NSData()
        if (!(data.valueForKey("picture") is  NSNull) && (data.valueForKey("picture") as! NSArray).count > 0){
             picture = data.valueForKey("picture") as! NSData
        }
        
        var user = User()
        if((data.valueForKey("user") as? NSDictionary) != nil){
            user = User.fromJSON(data.valueForKey("user") as! NSDictionary)
        }
        
        var LoginInfo = Credential()
        if((data.valueForKey("LoginInfo") as? NSDictionary) != nil){
            LoginInfo = Credential.FromJSON(data.valueForKey("LoginInfo") as! NSDictionary)
        }
        var accounts = [BankAccount]()
        if((data.valueForKey("accounts") as? NSArray) != nil){
            for account in (data.valueForKey("accounts") as! NSArray){
                if((account as? NSDictionary) != nil){
                    accounts.append(BankAccount.fromJSON(account as! NSDictionary))
                }
            }
        }
        return Profile(id: id, Nickname: nickname, picture: picture, user: user, LoginInfo: LoginInfo, accounts: accounts) 
        } else {
            return Profile()
        }
        
    }
    
    static func toJSON(profile : Profile) -> [String: AnyObject]{
        let jsonObject : [String: AnyObject] = [
            "id" : profile.id,
            "Nickname" : profile.Nickname,
            "accounts" : "",
            "LoginInfo" : Credential.ToJSON(profile.LoginInfo),
            "user" : User.toJSON(profile.user),
            //"picture" : profile.picture.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        ]
        return jsonObject
    }
    
}