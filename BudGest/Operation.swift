//
//  Operation.swift
//  BudGest
//
//  Created by Renaud Chardin on 04/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class Operation {
    
    var id : Int = -1
    var amount : Double = 0.0
    var category : Category = Category()
    var bankAccountId : Int = -1
    var operationName : String = ""
    var operationDate : NSDate = NSDate()
    var recipient : String = ""
    
    init(){
        
    }
    
    init(id : Int,amount : Double,category : Category,bankAccountId : Int, operationName : String, operationDate : NSDate, recipient : String){
        
        self.id = id
        self.amount = amount
        self.category = category
        self.bankAccountId = bankAccountId
        self.operationName = operationName
        self.operationDate = operationDate
        self.recipient = recipient
    }
    
    static func fromJSON(data : NSDictionary) -> Operation{
        
        let id = data.valueForKey("id") as! Int
        let amount = data.valueForKey("amount") as! Double
        var category = Category()
        if (( data.valueForKey("category") as? NSDictionary) != nil){
            category = Category.fromJSON( data.valueForKey("category") as! NSDictionary)
        }
        let bankAccount = data.valueForKey("bankAccountId") as! Int
        
        let opName = data.valueForKey("operationName") as! String
        let rec = data.valueForKey("recipient") as! String
        
        
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy/MM/dd hh:mm:ss"
        
        let opd = data.valueForKey("operationDate") as! String
        
        var opDate : NSDate = NSDate()
        if(!opd.isEmpty){
            opDate = dateformatter.dateFromString(opd)!
        }
        
        
        return Operation(id: id, amount: amount, category: category, bankAccountId: bankAccount, operationName: opName,operationDate: opDate,recipient: rec)
    }
    
    static func toJSON(operation : Operation) -> [String: AnyObject]{
        
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy/MM/dd hh:mm:ss"
        
        let jsonObject : [String: AnyObject] = [
            "id" : operation.id,
            "amount" : operation.amount,
            "category" : Category.toJSON(operation.category),
            "bankAccountId" : operation.bankAccountId,
            "operationName" : operation.operationName,
            "recipient" : operation.recipient,
            "operationDate" : dateformatter.stringFromDate(operation.operationDate)
        ]
        return jsonObject
    }
    
}