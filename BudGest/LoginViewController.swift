//
//  LoginViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 31/10/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var pwdValue: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func loginTapped(sender: SpringButton) {
        
        sender.animate()
        
        let username:NSString = userName.text!
        let password:NSString = pwdValue.text!
        
        if ( username.isEqualToString("") || password.isEqualToString("") ) {
            
            AlertManager.showAlert("Erreur lors de l'identification", message: "Veuillez saisir un login et un mot de passe", controler: self)
            
        } else {
            
            RestWebServiceConnector.getProfileByLogin({ (json, err) -> Void in
                
                if(err == nil){
                    
                    do {
                        let dic: NSDictionary = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        CommonProperties.profile = Profile.fromJSON(dic)
                        if(CommonProperties.profile.id == -1){
                            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                                AlertManager.showAlert("Login", message: "Aucun profile correspondant aux informations", controler: self)
                            }
                        } else {
                            if (password.isEqualToString(CommonProperties.profile.LoginInfo.password)){
                                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                                    let vc : UITabBarController = self.storyboard!.instantiateViewControllerWithIdentifier("MainTabControler") as! UITabBarController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                }
                            } else {
                                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                                    AlertManager.showAlert("Login", message: "Mot de passe incorrect", controler: self)
                                }
                            }
                            
                        }
                        
                    } catch {
                        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                            AlertManager.showAlert("Erreur", message: "Erreur lors de la récupération du profile", controler: self)
                        }
                    }
                    
                }
                
                }, login: username as String, pwd: password as String)
        }
    }
}
