//
//  OpenModeEnum.swift
//  BudGest
//
//  Created by Renaud Chardin on 18/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

public enum OpenMode {
    case Add
    case Edit
}