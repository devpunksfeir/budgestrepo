//
//  Category.swift
//  BudGest
//
//  Created by Renaud Chardin on 04/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class Category {
    
    var id: Int = -1
    var name : String = ""
    
    init(){
        
    }
    
    init(id : Int, name : String){
        self.id = id
        self.name = name
    }
    
    static func fromJSON(data : NSDictionary) -> Category{
        
        let id = data.valueForKey("id") as! Int
        let name = data.valueForKey("name") as! String
        
        return Category(id: id,name: name)
        
    }
    
    static func toJSON(category : Category) -> [String: AnyObject]{
        
        let jsonObject : [String: AnyObject] = [
            "id" : category.id,
            "name" : category.name
        ]
        return jsonObject
    }
    
    
    
}