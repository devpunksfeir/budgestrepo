//
//  CommonProperties.swift
//  BudGest
//
//  Created by Renaud Chardin on 16/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

struct CommonProperties {
    static var profile: Profile = Profile(id: 1, Nickname: "Charon", picture: NSData(), user: User(id: 1, lastname: "Chardin", firstname: "Renaud", birthday: NSDate()), LoginInfo: Credential(), accounts: [])
    
}