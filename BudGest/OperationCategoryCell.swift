//
//  OperationCategoryCell.swift
//  BudGest
//
//  Created by Renaud Chardin on 22/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class OperationCategoryCell: UITableViewCell {
    
    
    @IBOutlet var categoryNameLabel: UILabel!
    
    @IBOutlet var numberOfOperationLabel: UILabel!
    
    var cat : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
