//
//  AccountOperationViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 16/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class AccountOperationViewController: UITableViewController {
    
    var bankAccount : BankAccount = BankAccount()
    var cat : String = ""
    var operations : [Operation] = []
    var deleteOperation : NSIndexPath? = nil
    
    var sections = Dictionary<String, Array<Operation>>()
    var sortedSections = [String]()
    
    
    @IBOutlet var CategoryName: UINavigationItem!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        sections = Dictionary<String, Array<Operation>>()
        sortedSections = [String]()
        
        CategoryName.title = cat
        
        if(cat == "Toutes les opérations"){
            operations = bankAccount.operations
        } else {
            operations = bankAccount.operations.filter({$0.category.name==cat})
        }
        
        operations = operations.sort({$0.0.operationDate.compare($0.1.operationDate) == NSComparisonResult.OrderedDescending})
        
        for o : Operation in operations {
            let dateformatter = NSDateFormatter()
            dateformatter.dateFormat = "yyyy MM"
            let date:String = dateformatter.stringFromDate(o.operationDate)
            
            //if we don't have section for particular date, create new one, otherwise we'll just add item to existing section
            if self.sections.indexForKey(date) == nil {
                self.sections[date] = [o]
            }
            else {
                self.sections[date]!.append(o)
            }
            
            //we are storing our sections in dictionary, so we need to sort it
            self.sortedSections = self.sections.keys.sort({$0.0.compare( $0.1) == NSComparisonResult.OrderedDescending})
            
        }
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return operations.count
        return sections[sortedSections[section]]!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("operationCell", forIndexPath: indexPath) as! OperationCell
        
        let op = sections[sortedSections[indexPath.section]]![indexPath.row]
        //let op = operations[indexPath.row]
        
        cell.OperationNameLabel.text = op.operationName
        //cell.OperationCategoryLabel.text = op.category.name
        cell.OperationAmountLabel.text = String(op.amount)
        
        cell.CategoryLabel.text =  (cat == "Toutes les opérations") ?  op.category.name : ""
        
        
        
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "dd-MM-yyyy"
        
        cell.OperationDateLabel.text = dateformatter.stringFromDate(op.operationDate)
        
        
        
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "MM yyyy"
        let op = sections[sortedSections[section]]![0]
        return dateformatter.stringFromDate(op.operationDate)
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete") { (action, indexPath) in
            self.deleteOperation = indexPath
            self.deleteCurrentOperation()
        }
        
        let edit = UITableViewRowAction(style: .Normal, title: "Edit") { (action, indexPath) in
            
            let operation = self.sections[self.sortedSections[indexPath.section]]![indexPath.row] //  self.operations[indexPath.row]
            self.editOperation(operation)
        }
        
        edit.backgroundColor = UIColor.blueColor()
        
        return [delete, edit]
    }
    
    func editOperation(operation : Operation){
        let vc : AddOperationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("AddOperationVC") as! AddOperationViewController
        vc.operation = operation
        vc.openMode = OpenMode.Edit
        vc.modalPresentationStyle = .Popover
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func deleteCurrentOperation(){
        if (deleteOperation != nil) {
            
            
            
            let op : Operation =  self.sections[self.sortedSections[(deleteOperation?.section)!]]![(deleteOperation?.row)!]
            
            
            //self.operations[deleteOperation!.row]
            RestWebServiceConnector.deleteOperation({(json, error) -> Void in
                if(error == nil){
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        self.tableView.beginUpdates()
                        
                        
                        //self.sections = Dictionary<String, Array<Operation>>()
                        //self.sortedSections = [String]()
                        
                        var o  = self.operations.indexOf({$0.id == op.id})
                        
                        
                        let dateformatter = NSDateFormatter()
                        dateformatter.dateFormat = "yyyy MM"
                        let date:String = dateformatter.stringFromDate(self.operations[o!].operationDate)
                        
                        self.operations.removeAtIndex(o!)
                        
                        let s = self.sections[date]?.indexOf({$0.id == op.id})
                        self.sections[date]?.removeAtIndex(s!)
                        
                        o = self.bankAccount.operations.indexOf({$0.id == op.id})
                        self.bankAccount.operations.removeAtIndex(o!)
                        
                        if self.sections[date]?.count == 0 {
                            self.sortedSections.removeAtIndex(self.sortedSections.indexOf(date)!)
                            self.sections.removeValueForKey(date)
                            self.tableView.deleteSections(NSIndexSet(index: (self.deleteOperation?.section)!), withRowAnimation: .Automatic)
                        } else {
                            self.tableView.deleteRowsAtIndexPaths([self.deleteOperation!], withRowAnimation: .Automatic)
                        }
                        
                        
                        
                        self.deleteOperation = nil
                        
                        self.tableView.endUpdates()
                        
                    }
                }
                }, operationId: op.id)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "OpenAddOperationSegue" {
            let svc = segue.destinationViewController as! AddOperationViewController
            svc.bankAccount = self.bankAccount
            
            if(cat == "Toutes les opérations"){
                svc.cat = ""
            } else {
                svc.cat = self.cat
            }
            
            
            
            
        }
    }
    
    @IBAction func done(segue:UIStoryboardSegue) {
        
        
        let vc = segue.sourceViewController as! AddOperationViewController
        
        
        
        switch vc.openMode {
            
        case OpenMode.Add :
            
            if(cat == "Toutes les opérations"){
                
                let dateformatter = NSDateFormatter()
                dateformatter.dateFormat = "yyyy MM"
                let date:String = dateformatter.stringFromDate(vc.operation.operationDate)
                
                bankAccount.operations.append(vc.operation)
                operations = bankAccount.operations.sort({$0.0.operationDate.compare($0.1.operationDate) == NSComparisonResult.OrderedDescending})
                
                let sectionAdd = self.sections.indexForKey(date) == nil
                //if we don't have section for particular date, create new one, otherwise we'll just add item to existing section
                if sectionAdd {
                    self.sections[date] = [vc.operation]
                }
                else {
                    self.sections[date]!.append(vc.operation)
                    self.sections[date] = self.sections[date]!.sort({$0.0.operationDate.compare($0.1.operationDate) == NSComparisonResult.OrderedDescending})
                }
                
                //we are storing our sections in dictionary, so we need to sort it
                self.sortedSections = self.sections.keys.sort({$0.0.compare( $0.1) == NSComparisonResult.OrderedDescending})
                
                
                
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    
                    if (sectionAdd ) {
                        if (self.sortedSections.count > 1){
                            self.tableView.insertSections(NSIndexSet(index: self.sortedSections.indexOf(date)!), withRowAnimation: .Automatic)
                        }
                        
                    } else {
                        let i = self.sections[date]?.indexOf({$0.id == vc.operation.id})
                        let indexPath = NSIndexPath(forRow: i!, inSection: self.sortedSections.indexOf(date)!)
                        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    }
                }
                
                
            } else if(vc.operation.category.name == self.cat ){
                
                let dateformatter = NSDateFormatter()
                dateformatter.dateFormat = "yyyy MM"
                let date:String = dateformatter.stringFromDate(vc.operation.operationDate)
                
                
                
                bankAccount.operations.append(vc.operation)
                operations = bankAccount.operations.filter({$0.category.name==cat}).sort({$0.0.operationDate.compare($0.1.operationDate) == NSComparisonResult.OrderedDescending})
                
                let sectionAdd = self.sections.indexForKey(date) == nil
                //if we don't have section for particular date, create new one, otherwise we'll just add item to existing section
                if sectionAdd  {
                    self.sections[date] = [vc.operation]
                }
                else {
                    self.sections[date]!.append(vc.operation)
                    self.sections[date] = self.sections[date]!.sort({$0.0.operationDate.compare($0.1.operationDate) == NSComparisonResult.OrderedDescending})
                }
                
                //we are storing our sections in dictionary, so we need to sort it
                self.sortedSections = self.sections.keys.sort({$0.0.compare( $0.1) == NSComparisonResult.OrderedDescending})
                
                
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    
                    if (sectionAdd ) {
                        if (self.sortedSections.count > 1){
                            self.tableView.insertSections(NSIndexSet(index: self.sortedSections.indexOf(date)!), withRowAnimation: .Automatic)
                        }
                        
                    } else {
                        let i = self.sections[date]?.indexOf({$0.id == vc.operation.id})
                        let indexPath = NSIndexPath(forRow: i!, inSection: self.sortedSections.indexOf(date)!)
                        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                    }
                }
            } else {
                
                let dateformatter = NSDateFormatter()
                dateformatter.dateFormat = "yyyy MM"
                let date:String = dateformatter.stringFromDate(vc.operation.operationDate)
                
                let sectionAdd = self.sections.indexForKey(date) == nil
                //if we don't have section for particular date, create new one, otherwise we'll just add item to existing section
                if sectionAdd {
                    self.sections[date] = [vc.operation]
                }
                else {
                    self.sections[date]!.append(vc.operation)
                }
                
                //we are storing our sections in dictionary, so we need to sort it
                self.sortedSections = self.sections.keys.sort({$0.0.compare( $0.1) == NSComparisonResult.OrderedDescending})
                
                
                bankAccount.operations.append(vc.operation)
                self.tableView.reloadData()
            }
            
            
            
            
            
            
        case OpenMode.Edit :
            
            sections = Dictionary<String, Array<Operation>>()
            sortedSections = [String]()
            
            CategoryName.title = cat
            
            if(cat == "Toutes les opérations"){
                operations = bankAccount.operations
            } else {
                operations = bankAccount.operations.filter({$0.category.name==cat})
            }
            
            operations = operations.sort({$0.0.operationDate.compare($0.1.operationDate) == NSComparisonResult.OrderedDescending})
            
            for o : Operation in operations {
                let dateformatter = NSDateFormatter()
                dateformatter.dateFormat = "yyyy MM"
                let date:String = dateformatter.stringFromDate(o.operationDate)
                
                //if we don't have section for particular date, create new one, otherwise we'll just add item to existing section
                if self.sections.indexForKey(date) == nil {
                    self.sections[date] = [o]
                }
                else {
                    self.sections[date]!.append(o)
                }
                
                //we are storing our sections in dictionary, so we need to sort it
                self.sortedSections = self.sections.keys.sort({$0.0.compare( $0.1) == NSComparisonResult.OrderedDescending})
                
            }
            
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.tableView.reloadData()
            }
        }
        
        
        
        
        
    }
    
    @IBAction func cancel(segue:UIStoryboardSegue) {
        
    }
    
    
}
