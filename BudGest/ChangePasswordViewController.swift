//
//  ChangePasswordViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 22/12/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    
    @IBOutlet var ActualPWDTextField: UITextField!
    
    
    
    @IBOutlet var ConfirmNewPWDTextField: UITextField!
    
    @IBOutlet var NewPWDTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        
        if(identifier == "SaveNewPWD"){
            
            if(ActualPWDTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != CommonProperties.profile.LoginInfo.password){
                    AlertManager.showAlert("Changement de mot de pase", message: "Erreur, mot de passe erroné", controler: self)
                    return false
            }
            
            if(ActualPWDTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == NewPWDTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())){
                AlertManager.showAlert("Changement de mot de pase", message: "Erreur, le nouveau mot de passe est identique à l'ancien", controler: self)
                return false
            }
            if(NewPWDTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != ConfirmNewPWDTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())){
                AlertManager.showAlert("Changement de mot de pase", message: "Erreur, les nouveaux mot de passe saisies sont différents", controler: self)
                return false
            }
            if((NewPWDTextField.text) != nil && NewPWDTextField.text?.length > 0){
                let credential = Credential()
                credential.id = CommonProperties.profile.LoginInfo.id
                credential.login = CommonProperties.profile.LoginInfo.login
                credential.password = NewPWDTextField.text!
                
                RestWebServiceConnector.updatePassword({(json, error) -> Void in
                    if(error == nil){
                        CommonProperties.profile.LoginInfo.password = credential.password
                        self.performSegueWithIdentifier("SaveNewPWD", sender: self)
                    }
                    
                    }, credetial: credential)
            } else {
                AlertManager.showAlert("Changement de mot de pase", message: "Erreur, le nouveau mot de passe saisie est vide", controler: self)
                return false
            }
            
            
            return false
        }
        
        return true
    }
    
}
