//
//  StartViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 02/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    
    @IBAction func ImageTapped(sender: SpringButton) {
        sender.animation = "fall"
        sender.animate()
        sender.animation = "squeezeDown"
        sender.animate()
    }
    
}
