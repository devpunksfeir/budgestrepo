//
//  SimulationController.swift
//  BudGest
//
//  Created by François Bauché on 05/12/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class SimulationController: UIViewController {
    
    @IBOutlet weak var SegmentedControl1: UISegmentedControl!
   
    
    @IBOutlet weak var SliderTaux: UISlider!
    
    @IBOutlet weak var TextBoxTaux: UITextField!
    
    @IBOutlet weak var SliderMontant: UISlider!
    
    @IBOutlet weak var TextBoxMontant: UITextField!
    
    @IBOutlet weak var SliderDuree: UISlider!
    
    @IBOutlet weak var TextBoxDuree: UITextField!
    
    @IBOutlet weak var SliderMensualite: UISlider!
    
    @IBOutlet weak var TextBoxMensualite: UITextField!
    
    @IBAction func SCValueChanged(sender: AnyObject) {
        
        if(SegmentedControl1.selectedSegmentIndex == 0)
        {
            TextBoxTaux.text = "2.1";
            SliderTaux.setValue(2.1, animated: true);
        }
        else if(SegmentedControl1.selectedSegmentIndex == 1)
        {
            TextBoxTaux.text = "4.5";
            SliderTaux.setValue(4.5, animated: true);
        }
        
        TauxChanged(SliderTaux);
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func calculMensualite(C:Double, t:Double, n:Double) -> Double {
        
        let numerateur = C * (t/12);
        let denominateur = 1-(pow(1+(t/12),-n));
        
        return numerateur/denominateur;
        
    }
    
    
    @IBAction func TextBoxTauxChanged(sender: AnyObject) {
        let nf = NSNumberFormatter();
        SliderTaux.value =  (nf.numberFromString(TextBoxTaux.text!)?.floatValue)!;
        TauxChanged(SliderTaux);
    }
    
   
    
    @IBAction func TauxChanged(sender: AnyObject) {
        
        var taux : Float =  sender.value;
        taux = Float(round(100 * taux)/100);
        TextBoxTaux.text = ("\(taux)");
        
        let nf = NSNumberFormatter()
        
        TextBoxMensualite.text = nf.stringFromNumber(calculMensualite(Double(TextBoxMontant.text!)!, t: Double(TextBoxTaux.text!)!/100, n: Double(TextBoxDuree.text!)!*12));
        
        SliderMensualite.value =  (nf.numberFromString(TextBoxMensualite.text!)?.floatValue)!;
    }
    
    
    
    @IBAction func TextBoxMontantChanged(sender: AnyObject) {
        let nf = NSNumberFormatter();
        SliderMontant.value =  (nf.numberFromString(TextBoxMontant.text!)?.floatValue)!;
        MontantChanged(SliderMontant);
    }
    
    
    @IBAction func MontantChanged(sender: AnyObject) {
        
        
        let intervalle = 1000;
        let incomeValue = Int(sender.value / Float(intervalle) ) * intervalle;
        TextBoxMontant.text = ("\(incomeValue)");
        
        let nf = NSNumberFormatter()
     
        TextBoxMensualite.text = nf.stringFromNumber(calculMensualite(Double(TextBoxMontant.text!)!, t: Double(TextBoxTaux.text!)!/100, n: Double(TextBoxDuree.text!)!*12));
        
        SliderMensualite.value =  (nf.numberFromString(TextBoxMensualite.text!)?.floatValue)!;
        
    }
    
    @IBAction func TextBoxDureeChanged(sender: AnyObject) {
        let nf = NSNumberFormatter();
        SliderDuree.value =  (nf.numberFromString(TextBoxDuree.text!)?.floatValue)!;
        DureeChanged(SliderDuree);
    }
    
    
    @IBAction func DureeChanged(sender: AnyObject) {
        let intervalle = 1;
        let duree = Int(sender.value / Float(intervalle) ) * intervalle;
        
        TextBoxDuree.text = ("\(duree)");
        
        let nf = NSNumberFormatter()
        
        TextBoxMensualite.text = nf.stringFromNumber(calculMensualite(Double(TextBoxMontant.text!)!, t: Double(TextBoxTaux.text!)!/100, n: Double(TextBoxDuree.text!)!*12));
        
        SliderMensualite.value =  (nf.numberFromString(TextBoxMensualite.text!)?.floatValue)!;
        

    }
  
    @IBAction func TextBoxMensualiteChanged(sender: AnyObject) {
        let nf = NSNumberFormatter();
        SliderMensualite.value =  (nf.numberFromString(TextBoxMensualite.text!)?.floatValue)!;
        MensualiteChanged(SliderMensualite);
    }
    
    @IBAction func MensualiteChanged(sender: AnyObject) {
        
        
        let intervalle = 1;
        let mensualite = Int(sender.value / Float(intervalle) ) * intervalle;
        
        TextBoxMensualite.text = ("\(mensualite)");
        
        let nf = NSNumberFormatter()
        
        TextBoxMontant.text = nf.stringFromNumber(calculMontant(Double(TextBoxMensualite.text!)!, t: Double(TextBoxTaux.text!)!/100, n: Double(TextBoxDuree.text!)!*12));
        
        
        SliderMontant.value =  (nf.numberFromString(TextBoxMontant.text!)?.floatValue)!;
        
        
    }
    
    func calculMontant(m:Double, t:Double, n:Double) -> Double {
        
        let numerateur = m * (1-(pow(1+(t/12),-n)));
        let denominateur = (t/12);
        
        return numerateur/denominateur;
        
    }
    
    
    

}