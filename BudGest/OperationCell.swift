//
//  OperationCell.swift
//  BudGest
//
//  Created by Renaud Chardin on 23/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class OperationCell: UITableViewCell {

    @IBOutlet var OperationNameLabel: UILabel!
    
    @IBOutlet var OperationCategoryLabel: UILabel!
    
    @IBOutlet var OperationAmountLabel: UILabel!
    
    @IBOutlet var OperationDateLabel: UILabel!
    
    @IBOutlet var CategoryLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
