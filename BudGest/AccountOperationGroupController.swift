//
//  AccountOperationGroupController.swift
//  BudGest
//
//  Created by Renaud Chardin on 22/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class AccountOperationGroupController: UITableViewController {
    
    @IBOutlet var AccountNameTitle: UINavigationItem!
    
    var bankAccount : BankAccount = BankAccount()
    
    var categories : [String: [Operation]] = [:]
    
    var selectCategory : Category = Category()
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        AccountNameTitle.title = bankAccount.name
        loadCategories()
        tableView.reloadData()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->   UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("categoryCell", forIndexPath: indexPath) as! OperationCategoryCell
        
        let key : String = Array(categories.keys)[indexPath.row]
        
        cell.categoryNameLabel.text = key
        let tmp = String(categories[key]!.map({$0.amount}).reduce(0, combine: +))
        cell.numberOfOperationLabel.text = tmp + " " + self.bankAccount.devise
        
        cell.cat = key
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    
    func loadCategories(){
        categories["Toutes les opérations"] = bankAccount.operations
        
        let cat : [String: [Operation]] = bankAccount.operations.categorise({$0.category.name})
        for element in cat.sort({ $0.0.compare($1.0) == .OrderedAscending }) {
            categories[element.0] = element.1
        }
        categories.removeValueForKey("")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        switch segue.identifier {
        case "DisplayCategory"? :
            let cell = sender as! OperationCategoryCell
            let svc = segue.destinationViewController as! AccountOperationViewController
            svc.bankAccount = bankAccount
            svc.cat = cell.cat
        default : break
            
        }
    }
    
    
    
    
}


