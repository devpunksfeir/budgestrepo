//
//  Bank.swift
//  BudGest
//
//  Created by Renaud Chardin on 03/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class Bank {
    
    var id : Int = -1
    var name : String = ""
    var bic : String = ""
    var address : String = ""
    var loginInfo : Credential = Credential()
    
    init(){
        
    }
    
    init(id : Int,name : String, bic : String, address : String, log : Credential){
        self.id = id
        self.name = name
        self.bic = bic
        self.address = address
        self.loginInfo = log
    }
    
    static func fromJSON(data : NSDictionary) -> Bank {
        let id : Int = data.valueForKey("id") as! Int
        let name : String = data.valueForKey("Name") as! String
        let bic : String = data.valueForKey("Bic") as! String
        let address : String = data.valueForKey("Address") as! String
        var login = Credential()
        let t = data.valueForKey("LoginInfo")
        print(t)
        if((data.valueForKey("LoginInfo") as?  NSDictionary) != nil){
            login = Credential.FromJSON(data.valueForKey("LoginInfo") as! NSDictionary)
        }
        
        return Bank(id: id,name: name,bic: bic,address: address,log: login)
    }
    
    static func toJSON(bank : Bank) ->[String: AnyObject]{
        let jsonObject : [String : AnyObject] = [
            "id" : bank.id,
            "Name" : bank.name,
            "Bic" : bank.bic,
            "Address" : bank.address,
            "LoginInfo" : Credential.ToJSON(bank.loginInfo)
        ]
        return jsonObject
    }
    
}
