//
//  ViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 31/10/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func test(sender: AnyObject) {
        let rest : RestManager = RestManager()
        
        
        let addBank : Bank = Bank(id: 13, name: "test add", bic: "bic11", address: "adress11", log: Credential(id: 12, login: "charon", pwd: "test", key: "test"))
        
        let data = Bank.toJSON(addBank)
        
        rest.makeHTTPPostRequest({ (json, err) -> Void in
            }, url: "http://10.211.55.4:8733/BudGest/createbank", body: data)
        
        
        rest.makeHTTPGetRequest({ (json, err) -> Void in
            
            do{
                
                
                let dic: NSArray = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                
                print( (dic[0] as! NSDictionary).valueForKey("Name") as! String)
                
                print(dic.description)
                
                let bank : Bank = Bank.fromJSON(dic[0] as! NSDictionary)
                print(bank.name)
                
            } catch{
                
            }
            
            
            }, url: "http://10.211.55.4:8733/BudGest/GetBanks")
        
        
        
    }
}

