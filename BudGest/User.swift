//
//  User.swift
//  BudGest
//
//  Created by Renaud Chardin on 04/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import Foundation

class User {
    
    var id : Int = -1
    var lastName : String = ""
    var firstName : String = ""
    var birthday : NSDate = NSDate()
    
    init(){
        
    }
    
    init(id : Int,lastname : String, firstname : String, birthday : NSDate){
        self.id = id
        self.lastName = lastname
        self.firstName = firstname
        self.birthday = birthday
    }
    
    static func fromJSON(data : NSDictionary) -> User{
        let id = data.valueForKey("id") as! Int
        let lastname = data.valueForKey("LastName") as! String
        let firstname = data.valueForKey("Firstname") as! String
        let birthday = NSDate() //data.valueForKey("birthday") as! NSDate
        
        return User(id: id, lastname: lastname, firstname: firstname, birthday: birthday)
    }
    
    static func toJSON(user : User) -> [String: AnyObject]{
        
        //let dateformatter = NSDateFormatter()
        
        //dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        let jsonObject : [String: AnyObject] = [
            "id" : user.id,
            "LastName" : user.lastName,
            "Firstname" : user.firstName,
        ]
        return jsonObject
    }
    
}