//
//  BankAccountCell.swift
//  BudGest
//
//  Created by Renaud Chardin on 17/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class BankAccountCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var bankNameLabel: UILabel!
    
    @IBOutlet var ammountLabel: UILabel!
    
    var bankAccount : BankAccount = BankAccount()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
