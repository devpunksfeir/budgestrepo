//
//  AddOperationViewController.swift
//  BudGest
//
//  Created by Renaud Chardin on 24/11/2015.
//  Copyright © 2015 Dev Punk. All rights reserved.
//

import UIKit

class AddOperationViewController: UIViewController, UITextFieldDelegate {
    
    var bankAccount : BankAccount = BankAccount()
    
    var operation : Operation = Operation()
    
    var openMode : OpenMode = OpenMode.Add
    
    @IBOutlet var CategorieTextbox: UITextField!
    
    var cat : String = ""
    
    @IBOutlet var TitleLabel: UILabel!
    
    @IBOutlet var OperationNameTextbox: UITextField!
    
    @IBOutlet var AmountTextbox: UITextField!
    
    @IBOutlet var OperationDateTextbox: UITextField!
    
    
    @IBOutlet var RecipientTextbox: UITextField!
    
    @IBOutlet var OperationCategory: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.AmountTextbox.delegate = self
        
        let toolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
        
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        
        toolBar.barStyle = UIBarStyle.BlackTranslucent
        
        toolBar.tintColor = UIColor.whiteColor()
        
        toolBar.backgroundColor = UIColor.blackColor()
        
        
        let todayBtn = UIBarButtonItem(title: "Aujourd'hui", style: UIBarButtonItemStyle.Plain, target: self, action: "tappedToolBarBtn:")
        
        let okBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "donePressed:")
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
        
        label.font = UIFont(name: "Helvetica", size: 12)
        
        label.backgroundColor = UIColor.clearColor()
        
        label.textColor = UIColor.whiteColor()
        
        label.text = "Date de l'opération"
        
        label.textAlignment = NSTextAlignment.Center
        
        let textBtn = UIBarButtonItem(customView: label)
        
        toolBar.setItems([todayBtn,flexSpace,textBtn,flexSpace,okBarBtn], animated: true)
        
        OperationDateTextbox.inputAccessoryView = toolBar
        
        CategorieTextbox.text = cat
        
        switch openMode {
        case OpenMode.Add :
            TitleLabel.text = "Ajout d'une opération"
        case OpenMode.Edit :
            TitleLabel.text = "Modification de l'opération"
            bindOperation()
        }
    }
    
    func textField(textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        // We ignore any change that doesn't add characters to the text field.
        // These changes are things like character deletions and cuts, as well
        // as moving the insertion point.
        //
        // We still return true to allow the change to take place.
        if string.characters.count == 0 {
            return true
        }
        
        // Check to see if the text field's contents still fit the constraints
        // with the new content added to it.
        // If the contents still fit the constraints, allow the change
        // by returning true; otherwise disallow the change by returning false.
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        return prospectiveText.containsOnlyCharactersIn("-0123456789.")
        
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func bindOperation(){
        OperationNameTextbox.text = operation.operationName
        AmountTextbox.text = String(operation.amount)
        OperationCategory.text = operation.category.name
        RecipientTextbox.text = operation.recipient
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        OperationDateTextbox.text = dateFormatter.stringFromDate(operation.operationDate)
    }
    
    @IBAction func operationDateEditing(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        OperationDateTextbox.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    func donePressed(sender: UIBarButtonItem) {
        
        OperationDateTextbox.resignFirstResponder()
        
    }
    
    func tappedToolBarBtn(sender: UIBarButtonItem) {
        
        let dateformatter = NSDateFormatter()
        
        dateformatter.dateStyle = NSDateFormatterStyle.ShortStyle
        
        dateformatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        OperationDateTextbox.text = dateformatter.stringFromDate(NSDate())
        
        OperationDateTextbox.resignFirstResponder()
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "SaveOperationSegue" {
            
            let dateformatter = NSDateFormatter()
            dateformatter.dateStyle = NSDateFormatterStyle.ShortStyle
            if !OperationDateTextbox.text!.isEmpty{
                operation.operationDate = dateformatter.dateFromString(OperationDateTextbox.text!)!
            }
            
            operation.operationName = OperationNameTextbox.text!
            operation.recipient = RecipientTextbox.text!
            operation.bankAccountId = self.bankAccount.id
            operation.category = Category(id: -1, name: OperationCategory.text!)
            
            if !AmountTextbox.text!.isEmpty{
                operation.amount = Double(AmountTextbox.text!)!
            }
            
            switch openMode {
            case OpenMode.Add :
                //save operation
                RestWebServiceConnector.createOperation({(json, error) -> Void in
                    if(error == nil){
                        let datastring = NSString(data: json, encoding:NSUTF8StringEncoding)
                        self.operation.id = Int(datastring as! String)!
                        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                            self.performSegueWithIdentifier("SaveOperationSegue", sender: self)
                        }
                    }
                    
                    }, operation: operation)
                
            case OpenMode.Edit :
                
                //update operation
                RestWebServiceConnector.updateOperation({(json, error) -> Void in
                    if(error == nil){
                        self.performSegueWithIdentifier("SaveOperationSegue", sender: self)
                        
                    }
                    
                    }, operation: operation)            }
            
            
            return false
        }
        return true
    }
    
}
